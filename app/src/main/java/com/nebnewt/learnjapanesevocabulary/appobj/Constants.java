package com.nebnewt.learnjapanesevocabulary.appobj;

/**
 * Created by Neb on 12.12.2017.
 */

public abstract class Constants {
public static final String japaneseToPolish="japaneseToPolish";
public static final String polishToJapanese="polishToJapanese";
public static final String kanjiToKana="kanjiToKana";
public static final String defaultDirection = "japaneseToPolish";
public static final Boolean defaultKana = false;

public static final String flashcards="flashcards";
public static final String quiz="quiz";
    public static final String direction="direction";
    public static final String mode="mode";
    public static final String kana="kana";

    public static final String API_BASE_URL = "http://dev.japankitty.halamix2.pl/api/v1/";
}
