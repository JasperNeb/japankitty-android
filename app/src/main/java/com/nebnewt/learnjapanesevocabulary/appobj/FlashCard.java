package com.nebnewt.learnjapanesevocabulary.appobj;

import java.util.ArrayList;

/**
 * Created by Neb on 18.12.2017.
 */

public class FlashCard {
    private String kanji;
    private String reading;
    private String polish;

    private ArrayList<String> elements;
    private ArrayList<String> labels;


    public FlashCard(Word w)
    {
        elements=new ArrayList<>();
        labels=new ArrayList<>();
        elements.add(w.getKanji());
        elements.add(w.getKana());
        elements.add(w.getPolish());
     //   this.kanji=w.getKanji();
     //   this.reading=w.getKana();
     //   this.polish=w.getPolish();
        generateWordLabels();
    }
    public FlashCard(Kanji w)
    {
        elements=new ArrayList<>();
        labels=new ArrayList<>();
        elements.add(w.getKanji());
        elements.add(w.getKunyomi());
        elements.add(w.getOnyomi());
        elements.add(w.getDefinition());
     //   this.kanji=w.getKanji();
       // this.reading="";
      //  this.polish=w.toString();
        generateKanjiLabels();
    }

    private void generateKanjiLabels()
    {
        labels.add("kanji");
        labels.add("kunyomi");
        labels.add("onyomi");
        labels.add("znaczenie");
    }

    private void generateWordLabels()
    {
        labels.add("kanji");
        labels.add("kana");
        labels.add("polski");


    }

    public String getKanji() {
        return kanji;
    }

    public void setKanji(String kanji) {
        this.kanji = kanji;
    }

    public String getReading() {
        return reading;
    }

    public void setReading(String reading) {
        this.reading = reading;
    }

   // public String getPolish() {
   //     return polish;
 //   }

    public void setPolish(String polish) {
        this.polish = polish;
    }

    public ArrayList<String> getElements() {
        return elements;
    }

    public ArrayList<String> getLabels() {
        return labels;
    }
}
