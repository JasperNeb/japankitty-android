package com.nebnewt.learnjapanesevocabulary.appobj;

/**
 * Created by Neb on 05.12.2017.
 */

public class Course {
    private int id;
    private String name;
    private String file;
    private String type;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
