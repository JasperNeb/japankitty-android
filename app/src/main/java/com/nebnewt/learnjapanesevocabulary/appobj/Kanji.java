package com.nebnewt.learnjapanesevocabulary.appobj;

import android.util.Log;

/**
 * Created by Neb on 09.12.2017.
 */

public class Kanji {
    private int id;
    private String kanji;
    private String onyomi;
    private String kunyomi;
    private String definition;

    public String getDefinition() {
        return definition;
    }

    public void setDefinition(String meaning) {
        this.definition = meaning;
    }

    public Kanji(String kanji, String onyomi, String kunyomi, String definition)
    {
        this.kanji=kanji;
        this.onyomi=onyomi;
        this.kunyomi=kunyomi;
        this.definition=definition;
        Log.i("inKanji", this.definition);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKanji() {
        return kanji;
    }


    public String getOnyomi() {
        return onyomi;
    }



    public String getKunyomi() {
        return kunyomi;
    }

    @Override
    public String toString()
    {
        return "kunyomi: "+kunyomi+"\nonyomi: "+onyomi+"\nznaczenie: "+definition;
    }

}
