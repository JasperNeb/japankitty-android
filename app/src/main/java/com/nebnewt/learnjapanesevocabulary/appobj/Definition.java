package com.nebnewt.learnjapanesevocabulary.appobj;

/**
 * Created by Neb on 06.12.2017.
 */

public class Definition {
    private int id;
    private String word;
    private String definition;
    public Definition(String word, String definition)
    {
        this.word=word;
        this.definition=definition;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getDefinition() {
        return definition;
    }

    public void setDefinition(String definition) {
        this.definition = definition;
    }
}
