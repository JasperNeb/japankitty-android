package com.nebnewt.learnjapanesevocabulary.appobj;

public class FlashCard2 {
    private final String front;
    private final String back;
    private String shown;

    public FlashCard2(String front, String back)
    {
        this.front=front;
        this.back=back;
        this.shown=front;
    }

    public void flip()
    {
        if(shown.equals(front))
        {
            shown=back;
        }
        else
        {
            shown=front;
        }
    }

    public String getFront() {
        return front;
    }

    public String getBack() {
        return back;
    }

    public String getShown() {
        return shown;
    }
}
