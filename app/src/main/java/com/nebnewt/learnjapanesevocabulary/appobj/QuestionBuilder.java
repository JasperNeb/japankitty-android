package com.nebnewt.learnjapanesevocabulary.appobj;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.nebnewt.learnjapanesevocabulary.appobj.Constants;
import com.nebnewt.learnjapanesevocabulary.appobj.Definition;
import com.nebnewt.learnjapanesevocabulary.appobj.Kanji;
import com.nebnewt.learnjapanesevocabulary.appobj.Question;
import com.nebnewt.learnjapanesevocabulary.appobj.Word;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/**
 * Created by Neb on 29.10.2017.
 */

public class QuestionBuilder {
    private final int NO_OF_ANSWERS=5;
    public QuestionBuilder()
    {

    }
    public Question generate_question(Word w, ArrayList<Word> words, Context context)
    {
        Question q;
        ArrayList<String> answers=new ArrayList<>();
        String correct;
        String question;
        String questionkana="";
        Random r=new Random();
        SharedPreferences sharedPreferences= PreferenceManager.getDefaultSharedPreferences(context);

       if(sharedPreferences.getString(Constants.direction, Constants.defaultDirection).equals(Constants.japaneseToPolish))
       {
           question=w.getKanji();

           if(sharedPreferences.getBoolean(Constants.kana, Constants.defaultKana))
           {
               questionkana=w.getKana();
           }
           else
           {
               questionkana=" ";
           }
           correct=w.getPolish();
           answers.add(correct);

           for(int i=0 ; i<NO_OF_ANSWERS-1 ; i++)
           {
               int randint=r.nextInt(words.size());
               String answer=words.get(randint).getPolish();
               if(answers.contains(answer))
               {
                   i--;
               }
               else
               {
                   answers.add(answer);
               }
           }
       }
       else if(sharedPreferences.getString(Constants.direction, Constants.defaultDirection).equals(Constants.polishToJapanese))
       {
           question=w.getPolish();
           if(sharedPreferences.getBoolean(Constants.kana, Constants.defaultKana))
           {
               correct=w.getKana();
               answers.add(correct);
               for(int i=0 ; i< NO_OF_ANSWERS-1 ; i++)
               {
                   int randint=r.nextInt(words.size());
                   String answer=words.get(randint).getKana();
                   if(answers.contains(answer))
                   {
                       i--;
                   }
                   else
                   {
                       answers.add(answer);
                   }
               }
           }
           else
           {
               correct=w.getKanji();
               answers.add(correct);
               for(int i=0 ; i< NO_OF_ANSWERS-1 ; i++)
               {
                   int randint=r.nextInt(words.size());
                   String answer=words.get(randint).getKanji();
                   if(answers.contains(answer))
                   {
                       i--;
                   }
                   else
                   {
                       answers.add(answer);
                   }
               }
           }
       }
       else if(sharedPreferences.getString(Constants.direction, Constants.defaultDirection).equals(Constants.kanjiToKana))
       {
           question=w.getKanji();
           correct=w.getKana();
           answers.add(correct);
           for(int i=0 ; i< NO_OF_ANSWERS-1 ; i++)
           {
               int randint=r.nextInt(words.size());
               String answer=words.get(randint).getKana();
               if(answers.contains(answer))
               {
                   i--;
               }
               else
               {
                   answers.add(answer);
               }
           }
       }
       else
       {
           question="nieprawidlowe opcje";
           correct="nieprawidlowe opcje";
       }


        Collections.shuffle(answers);

        q=new Question(answers , question, correct, questionkana);

        return q;
    }

    public Question generate_question(Kanji kanji, ArrayList<Kanji> kanjis)
    {
        Question question;
        ArrayList<String> answers= new ArrayList<>();
        String correct=kanji.toString()+" ";
        Random r;
        r=new Random();
        answers.add(correct);
        for(int i=0 ; i< NO_OF_ANSWERS-1 ; i++)
        {
            int rand=r.nextInt(kanjis.size());
            if(answers.contains(kanjis.get(rand).toString()))
            {
                i--;

            }
            else
            {
                answers.add(kanjis.get(rand).toString());
            }
        }
        Collections.shuffle(answers);

        question = new Question(answers , kanji.getKanji(), correct, "");
        return question;
    }


    public Question generate_question(Definition definition, ArrayList<Definition> definitions, Context context)
    {
        Question question;
        ArrayList<String> answers= new ArrayList<>();
        String correct;
        Random r;
        r=new Random();
        Log.i("QUESTION_BUILDER_TAG", definition.getDefinition());
        correct=definition.getDefinition();

        for(int i=0 ; i< NO_OF_ANSWERS-1 ; i++)
        {
            int rand=r.nextInt(definitions.size());
            if(definitions.get(rand).equals(definition) || answers.contains(definitions.get(rand).getDefinition()))
            {
                i--;

            }
            else
            {
                answers.add(definitions.get(rand).getDefinition());
            }
        }
        answers.add(correct);
        Collections.shuffle(answers);

        question = new Question(answers , definition.getWord(), definition.getDefinition(), "");
        return question;


    }



}
