package com.nebnewt.learnjapanesevocabulary.appobj;

/**
 * Created by Neb on 23.10.2017.
 */

public class Word {
    private int id;
    private String kana;
    private String kanji;
    private String polish;
    public Word(int id, String kana, String kanji, String polish)
    {
        this.kana=kana;
        this.kanji=kanji;
        this.id=id;
        this.polish=polish;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKanji() {
        return kanji;
    }

    public void setKanji(String kanji) {
        this.kanji = kanji;
    }

    public String getPolish() {
        return polish;
    }

    public void setPolish(String polish) {
        this.polish = polish;
    }

    public String getKana() {
        return kana;
    }

    public void setKana(String kana) {
        this.kana = kana;
    }

}
