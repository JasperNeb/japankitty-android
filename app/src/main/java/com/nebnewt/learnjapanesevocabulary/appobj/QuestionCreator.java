package com.nebnewt.learnjapanesevocabulary.appobj;

import com.nebnewt.learnjapanesevocabulary.appobj.Question;

/**
 * Created by Neb on 10.12.2017.
 */

public interface QuestionCreator {
    Question createQuestion();
}
