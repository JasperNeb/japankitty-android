package com.nebnewt.learnjapanesevocabulary.appobj;

import java.util.ArrayList;


public class Question {

    ArrayList<String> answers;
    String question;
    String correct;
    String userAnswer;
    String questionkana;

    public Question(ArrayList<String>answers, String question, String correct, String questionkana)
    {
        this.answers=new ArrayList<>(answers);
        this.question=question;
        this.correct=correct;
        this.questionkana=questionkana;
    }

    public boolean isCorrect(String s)
    {
        if(s==this.correct)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public ArrayList<String> getAnswers() {
        return answers;
    }

    public void setAnswers(ArrayList<String> answers) {
        this.answers = answers;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getCorrect() {
        return correct;
    }

    public void setCorrect(String correct) {
        this.correct = correct;
    }

    public String getUserAnswer() {
        return userAnswer;
    }

    public void setUserAnswer(String userAnswer) {
        this.userAnswer = userAnswer;
    }

    public String getQuestionkana() {
        return questionkana;
    }

}
