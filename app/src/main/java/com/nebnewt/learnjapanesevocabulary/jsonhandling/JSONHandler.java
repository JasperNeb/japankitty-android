package com.nebnewt.learnjapanesevocabulary.jsonhandling;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import com.nebnewt.learnjapanesevocabulary.appobj.Definition;
import com.nebnewt.learnjapanesevocabulary.appobj.Kanji;
import com.nebnewt.learnjapanesevocabulary.appobj.Word;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class JSONHandler {

    /**
     * Gets JSONArray from assets txt file
     * @param path path to JSON file in assets
     * @param myContext Application context, I prefer getApplicatioContext() in here
     * @return JSONArray from selected file
     */
    static public JSONArray readJSONArrayFromAssets(String path, Context myContext) {
        AssetManager assetManager = myContext.getAssets();
        JSONArray jsonArray = new JSONArray();

        try {
            InputStream inputStream = assetManager.open(path);

            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder builder = new StringBuilder();
            {
                String line; //brackets to kill this helper later
                while ((line = reader.readLine()) != null) {
                    builder.append(line);
                }
            }
            inputStream.close();
            jsonArray = (JSONArray) new JSONTokener(builder.toString()).nextValue();

        } catch (IOException | JSONException e) {
            Log.e("error", e.getMessage());
        }

        return jsonArray;
    }
    static public ArrayList<Word> JsonToWord(JSONArray jsonArray) throws JSONException
    {
        ArrayList<Word> words= new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++)
            {
                JSONObject tempObject = jsonArray.getJSONObject(i);

                String tempKanji = tempObject.getString("kanji");
                String tempPolish = tempObject.getString("polish");
                String tempKana = tempObject.getString("hiragana");

                Word w=new Word(i, tempKana, tempKanji, tempPolish);

                words.add(w);
            }


        return words;
    }
    static public ArrayList<Definition> JsonToDefinition(JSONArray jsonArray) throws JSONException
    {
        ArrayList<Definition> definitions= new ArrayList<>();
        for(int i=0 ; i< jsonArray.length() ; i++)
        {
            JSONObject tempObject = jsonArray.getJSONObject(i);
            String word = tempObject.getString("word");
            String definition = tempObject.getString("definition");
            Log.i("JSONHANDLER_TAG", definition);


            Definition d = new Definition(word,definition);
            definitions.add(d);

        }
        return definitions;
    }

    static public ArrayList<Kanji> JsonToKanji(JSONArray jsonArray) throws JSONException
    {
        ArrayList<Kanji> definitions= new ArrayList<>();
        for(int i=0 ; i< jsonArray.length() ; i++)
        {
            JSONObject tempObject = jsonArray.getJSONObject(i);
            String kanji = tempObject.getString("kanji");
            String on = tempObject.getString("on");
            String kun = tempObject.getString("kun");
            String meaning=tempObject.getString("definition");
            Log.i("JSONHANDLER", meaning);
            Log.i("JSONHandler", kanji);

            Kanji d = new Kanji(kanji, on, kun, meaning);
            Log.i("inKanji", meaning);
            Log.i("JSONHANDLER", meaning);
            definitions.add(d);

        }
        return definitions;
    }
}
