package com.nebnewt.learnjapanesevocabulary.networking;

import com.nebnewt.learnjapanesevocabulary.appobj.User;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by Neb on 2018/01/13.
 */

public interface UserClient {
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST("login")
    Call<User> loginUser(@Body LoginData data);

}
