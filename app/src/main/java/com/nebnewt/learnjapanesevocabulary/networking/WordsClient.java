package com.nebnewt.learnjapanesevocabulary.networking;

import com.nebnewt.learnjapanesevocabulary.appobj.Word;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Neb on 19.12.2017.
 */

public interface WordsClient {
    @GET("vocabulary/{id}")
    Call<List<Word>> wordsList(
            @Path("id") String course
    );
}
