package com.nebnewt.learnjapanesevocabulary.networking;

import com.nebnewt.learnjapanesevocabulary.appobj.Course;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface CategoriesClient {
    @GET("courses/{type}")
    Call<List<Course>> coursesList(
            @Path("type") String course
    );
}
