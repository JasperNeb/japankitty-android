package com.nebnewt.learnjapanesevocabulary.networking;

/**
 * Created by Neb on 2018/01/14.
 */

public class LoginResult {
    private String message;
    private String token;

    public String getMessage() {
        return message;
    }

    public String getToken() {
        return token;
    }
}
