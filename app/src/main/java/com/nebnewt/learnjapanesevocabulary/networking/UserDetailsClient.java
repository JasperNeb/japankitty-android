package com.nebnewt.learnjapanesevocabulary.networking;

import com.nebnewt.learnjapanesevocabulary.appobj.UserDetails;

import retrofit2.Call;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface UserDetailsClient {
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST("get-details")
    Call<UserDetails> loginUser();//@Body MobileData mobile);
}
