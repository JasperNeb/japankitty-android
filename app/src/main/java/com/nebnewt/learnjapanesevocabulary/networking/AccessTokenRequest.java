package com.nebnewt.learnjapanesevocabulary.networking;

import android.os.AsyncTask;

import org.json.JSONObject;

import okhttp3.FormBody;
import okhttp3.RequestBody;

/**
 * Created by Neb on 2018/01/13.
 */

 public class AccessTokenRequest extends AsyncTask<Void, Void, String> {

    @Override
    protected String doInBackground(Void... voids) {
        String accessToken = null;
        OkHttpJsonRequest jsonRequest = new OkHttpJsonRequest();
        RequestBody requestBody = new FormBody.Builder()
                .add("grant_type", "password")
                .add("username", "bnk")
                .add("password", "bnk123")
                .build();
        try {
            JSONObject jsonObject = jsonRequest.post("http://192.168.1.100:24780/token", requestBody);
            if (!jsonObject.isNull("access_token")) {
                accessToken = jsonObject.getString("access_token");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return accessToken;
    }

    @Override
    protected void onPostExecute(String response) {
        super.onPostExecute(response);

    }
}

