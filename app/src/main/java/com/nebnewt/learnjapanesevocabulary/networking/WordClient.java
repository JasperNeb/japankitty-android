package com.nebnewt.learnjapanesevocabulary.networking;

import com.nebnewt.learnjapanesevocabulary.appobj.Word;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Neb on 13.11.2017.
 */

//http://japankitty.halamix2.pl/api/vocabulary/basic

public interface WordClient {
    @GET("vocabulary/{word}")
    Call<List<Word>> wordList(
            @Path("word") String word
    );
}
