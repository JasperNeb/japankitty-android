package com.nebnewt.learnjapanesevocabulary.networking;

/**
 * Created by Neb on 2018/01/13.
 */

public class MobileData {
    private String mobile;

    public MobileData() {
        this.mobile = "yup";
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
