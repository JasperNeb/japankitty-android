package com.nebnewt.learnjapanesevocabulary.networking;

import android.os.AsyncTask;
import android.util.Log;

import com.nebnewt.learnjapanesevocabulary.networking.AsyncResponse;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Neb on 29.11.2017.
 */

public class InternetConnectionChecker extends AsyncTask<URL, Integer, Boolean> {

    public AsyncResponse delegate = null;


    @Override
    protected Boolean doInBackground(URL... urls) {


        try {
            HttpURLConnection urlc = (HttpURLConnection) (new URL("http://japankitty.halamix2.pl").openConnection());
            urlc.setRequestProperty("User-Agent", "Test");
            urlc.setRequestProperty("Connection", "close");
            urlc.setConnectTimeout(1500);
            urlc.connect();
            return (urlc.getResponseCode() == 200);
        } catch (IOException e) {
            Log.e("STARTING", "Error checking internet connection", e);
            return false;

        }


    }

    @Override
    protected void onPostExecute(Boolean result)
    {
        Log.i("res", result.toString());
        delegate.processFinish(result);
        Log.i("delegate", delegate.toString());
    }

   /* public boolean hasActiveInternetConnection(Context context, URL url) {

        if (isNetworkAvailable(context)) {
            new InternetConnectionChecker().execute(url);
        } else {
            Log.d("STARTING", "No network available!");
            return false;
        }
        return false;
    }

    private boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        Log.i("wtfwtf", "dupa");
        return activeNetworkInfo != null;
    }

    @Override
    public void processFinish(Boolean hasInternetConnection)
    {
        //hasOAuthToken();
        if(hasInternetConnection)
        {

            Log.i("NETTTT, SMUTEK", "FAAAAAK");

        }
        else
        {
            Log.i("NIE MA NETA, SMUTEK", "FAAAAAK");

        }

    }*/
}
