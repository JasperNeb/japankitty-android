package com.nebnewt.learnjapanesevocabulary.networking;

/**
 * Created by Neb on 2018/01/13.
 */

public class LoginData {
    private String email;
    private String password;
    private String mobile;

    public LoginData(String email, String password) {
        this.email = email;
        this.password = password;
        this.mobile = "yup";
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
