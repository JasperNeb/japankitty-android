package com.nebnewt.learnjapanesevocabulary.networking;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class GetAsyncUserDetails extends AsyncTask<QueryData, String, String> {

    GetUserDetailsCallbackInterface callbackInterface;

    public GetAsyncUserDetails(GetUserDetailsCallbackInterface getUserDetailsCallbackInterface)
    {
        this.callbackInterface=getUserDetailsCallbackInterface;
    }

    protected String doInBackground(QueryData... data) {
        String stringi = "";
        Log.d("data", data[0].token);
        try {
            URL fuckingURL = new URL(data[0].baseURL + data[0].endpoint);
            HttpURLConnection urlConnection = (HttpURLConnection) fuckingURL.openConnection();
            urlConnection.setRequestMethod("POST");

            urlConnection.setReadTimeout(10000);
            urlConnection.setConnectTimeout(10000);
            urlConnection.setUseCaches(false);
            urlConnection.setAllowUserInteraction(false);

            urlConnection.setRequestProperty("Accept", "application/json");
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.setRequestProperty("Authorization", data[0].token);
            OutputStream os = urlConnection.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
            osw.write("{\"mobile\": \"true\"}");
            osw.flush();
            osw.close();
            os.close();

            try {
                BufferedReader bufferedReader = new BufferedReader(
                        new InputStreamReader(urlConnection.getInputStream())
                );
                StringBuilder stringBuilder = new StringBuilder();
                String fuckingLine;
                while ((fuckingLine = bufferedReader.readLine()) != null) {
                    stringBuilder.append(fuckingLine);
                }
                stringi = stringBuilder.toString();
                bufferedReader.close();
            } finally {
                urlConnection.disconnect();
            }
        } catch (Exception e) {

            Log.e("mam", e.getMessage());
        }
        return stringi;
    }

    protected void onPostExecute(String result) {
        Log.w("mam",result);
        this.callbackInterface.fetchDataCallback(result);
    }

}
