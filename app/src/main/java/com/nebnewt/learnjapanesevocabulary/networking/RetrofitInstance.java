package com.nebnewt.learnjapanesevocabulary.networking;

import com.nebnewt.learnjapanesevocabulary.appobj.Constants;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitInstance {

    private volatile static RetrofitInstance instance;
    private Retrofit retrofit;
   // private OkHttpClient client;

    private RetrofitInstance()
    {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        Retrofit.Builder builder =
                new Retrofit.Builder()
                        .baseUrl(Constants.API_BASE_URL)
                        .addConverterFactory(
                                GsonConverterFactory.create()
                        );

        retrofit = builder.client(httpClient.build()).build();
    }

    public static RetrofitInstance getInstance() {
        if (instance == null) {
            synchronized (RetrofitInstance.class) {
                if (instance == null) {
                    instance = new RetrofitInstance();
                }
            }
        }

        return instance;
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }
}
