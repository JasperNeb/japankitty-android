package com.nebnewt.learnjapanesevocabulary.networking;

/**
 * Created by Neb on 30.11.2017.
 */

public interface AsyncResponse {
    void processFinish(Boolean output);

}
