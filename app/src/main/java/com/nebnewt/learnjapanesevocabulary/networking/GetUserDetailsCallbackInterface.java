package com.nebnewt.learnjapanesevocabulary.networking;

public interface GetUserDetailsCallbackInterface {

    public void fetchDataCallback (String result);
}
