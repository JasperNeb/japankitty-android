package com.nebnewt.learnjapanesevocabulary.networking;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Neb on 2018/01/13.
 */

public class OkHttpJsonRequest {
    OkHttpClient client = new OkHttpClient();

    public JSONObject post(String url, RequestBody body) throws IOException, JSONException {
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        Response response = client.newCall(request).execute();
        return new JSONObject(response.body().string());
    }
}