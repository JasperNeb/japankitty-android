package com.nebnewt.learnjapanesevocabulary.networking;

import com.nebnewt.learnjapanesevocabulary.appobj.Progress;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public interface ProgressClient {
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @GET("progress")
    Call<Progress> getProgress(

    );
}
