package com.nebnewt.learnjapanesevocabulary.networking;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Neb on 2018/01/15.
 */

public interface MotdClient {

    @GET("api/motd")
    Call<String> motd(
            @Path("motd") String motdString
    );
}
