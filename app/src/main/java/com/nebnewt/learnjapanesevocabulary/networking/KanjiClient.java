package com.nebnewt.learnjapanesevocabulary.networking;

import com.nebnewt.learnjapanesevocabulary.appobj.Kanji;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Neb on 22.01.2018.
 */

public interface KanjiClient {
    @GET("kanji/{id}")
    Call<List<Kanji>> kanjiList(
            @Path("id") String kanjiString
    );
}
