package com.nebnewt.learnjapanesevocabulary.activities;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.nebnewt.learnjapanesevocabulary.appobj.Constants;
import com.nebnewt.learnjapanesevocabulary.appobj.Definition;
import com.nebnewt.learnjapanesevocabulary.jsonhandling.JSONHandler;
import com.nebnewt.learnjapanesevocabulary.appobj.Kanji;
import com.nebnewt.learnjapanesevocabulary.networking.KanjiClient;
import com.nebnewt.learnjapanesevocabulary.appobj.Question;
import com.nebnewt.learnjapanesevocabulary.appobj.QuestionBuilder;
import com.nebnewt.learnjapanesevocabulary.R;
import com.nebnewt.learnjapanesevocabulary.networking.RetrofitInstance;
import com.nebnewt.learnjapanesevocabulary.appobj.Word;
import com.nebnewt.learnjapanesevocabulary.networking.WordsClient;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QuizActivity extends Activity {


    public ArrayList<Question> questions;
    public int score=0;
    public boolean online=false;
    private int number_of_words;
    private boolean kana;

    private int ind=0;

    String filename;
    String id;
    String type;

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        score=0;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);

        sharedPreferences= PreferenceManager.getDefaultSharedPreferences(this);
        number_of_words = sharedPreferences.getInt("no_of_questions", 5);
        kana = sharedPreferences.getBoolean("kana", true);
        online= getIntent().getExtras().getBoolean("online", false);
        id = getIntent().getExtras().getString("id");
        filename=getIntent().getExtras().getString("FILENAME");
        type = getIntent().getExtras().getString("TYPE");
        Log.i("type", type);
        Log.i("quiz_online", Boolean.toString(online));
        Log.i("quiz filename", filename);
        questions = new ArrayList<>();

        if(!online)
        {

            load(filename, type);
            update();
        }
        else
        {
            Log.i("quiz aaa", "im online biczez");
            downloadQuestions();


        }

    }


    private void downloadQuestions()
    {
        RetrofitInstance retrofitInstance=RetrofitInstance.getInstance();




        if(type.equals("kanji"))
        {
            KanjiClient client =  retrofitInstance.getRetrofit().create(KanjiClient.class);

            Call<List<Kanji>> call =
                    client.kanjiList(id);

            call.enqueue(new Callback<List<Kanji>>() {
                @Override
                public void onResponse(Call<List<Kanji>> call, Response<List<Kanji>> response) {
                    Log.i("dupa", new Gson().toJson(response.body()));

                    ArrayList kanji;
                    kanji=new ArrayList<Kanji>(response.body());
                    generateQuestionFromKanji(kanji);
                    update();



                }

                @Override
                public void onFailure(Call<List<Kanji>> call, Throwable t) {
                    Log.i("no dupa no", "nie da sie");



                }
            });
        }
        else if(type.equals("vocabulary"))
        {
            WordsClient client =  retrofitInstance.getRetrofit().create(WordsClient.class);

            Call<List<Word>> call =
                    client.wordsList(id);

            call.enqueue(new Callback<List<Word>>() {
                @Override
                public void onResponse(Call<List<Word>> call, Response<List<Word>> response) {
                    Log.i("dupa23", new Gson().toJson(response.body()));

                    ArrayList <Word> words=new ArrayList<Word>(response.body());
                    generateQuestionsFromWords(words);
                    update();



                }

                @Override
                public void onFailure(Call<List<Word>> call, Throwable t) {
                    Log.i("no dupa no", "nie da sie");



                }
            });
        }

    }

    private void generateQuestionsFromWords(ArrayList <Word> words)
    {
        Collections.shuffle(words);
        Log.i("sizeofwords", Integer.toString(words.size()));

        for(int i=0 ; i< words.size(); i++ )
        {
            Question q;
            QuestionBuilder questionBuilder=new QuestionBuilder();
            q=questionBuilder.generate_question(words.get(i), words, this);
            questions.add(q);
        }
        number_of_words=words.size();
        Log.i("noofwords", Integer.toString(number_of_words));

    }
    private void generateQuestionFromKanji(ArrayList <Kanji> kanji)
    {
        Collections.shuffle(kanji);
        for(int i=0 ; i< kanji.size(); i++ )
        {
            Question q;
            QuestionBuilder questionBuilder=new QuestionBuilder();
            q=questionBuilder.generate_question(kanji.get(i), kanji);
            questions.add(q);
        }
        number_of_words=kanji.size();
    }

    private void load(String filename, String type)
    {

        try {
            JSONArray questionsJSONArray = JSONHandler.readJSONArrayFromAssets("categories/"+filename, getApplicationContext());
           // Log.i("log", questionsJSONArray.toString());
            if(type.equals("words"))
            {
                ArrayList<Word> words=new ArrayList<>();
                words=JSONHandler.JsonToWord(questionsJSONArray);

                Collections.shuffle(words);
                generateQuestionsFromWords(words);

            }
            else if(type.equals("definitions"))
            {

                ArrayList<Definition> definitions;
                definitions=JSONHandler.JsonToDefinition(questionsJSONArray);

                Collections.shuffle(definitions);

                for(int i=0 ; i<definitions.size() ; i++)
                {

                    Question q;
                    QuestionBuilder questionBuilder=new QuestionBuilder();
                    q=questionBuilder.generate_question(definitions.get(i), definitions, this);
                    questions.add(q);
                }
            }




        } catch (JSONException e) {
            Log.e("error", e.getMessage());
            finish();
        }
    }

    private Question losuj()
    {
        Question q;
        q=questions.get(ind);
        ind++;

        return q;
    }

    private void update() //TODO przeniesc buttony do layoutu
    {
        final Question q=losuj();
        //final ArrayList<String> user_answers=new ArrayList<String>();
        TextView t= new TextView(this);
        TextView pkt=new TextView(this);
        TextView kana=new TextView(this);

        t.setText(q.getQuestion());
        if(sharedPreferences.getString(Constants.direction, Constants.defaultDirection).equals(Constants.japaneseToPolish))
        {
            Log.i("HEJHEJ", "kanaaa");
            kana.setText(q.getQuestionkana());
        }


        t.setBackgroundResource(R.drawable.text);
        t.setGravity(Gravity.CENTER);
        t.setTextSize(40);
        t.setPadding(5,5,5,5);
        t.setTextColor(Color.BLACK);
        kana.setTextColor(Color.BLACK);
        kana.setGravity(Gravity.CENTER);
        kana.setTextSize(25);


        pkt.setGravity(Gravity.CENTER);
        pkt.setBackgroundResource(R.drawable.text);
        final LinearLayout llButtons = (LinearLayout) findViewById(R.id.layout_scroll);
        final LinearLayout llQuestionText=(LinearLayout) findViewById(R.id.layout_text);
        llQuestionText.addView(kana);

        llQuestionText.addView(t); //dodaje textview z pytaniem i z iloscia poprawnych odp w danycm pytaniu
        for(int i=0 ; i<q.getAnswers().size() ; i++)
        {
            final Button myButton = new Button(this);
            myButton.setText(q.getAnswers().get(i));
            myButton.setId(i);
            myButton.setTextSize(20);
            LinearLayout.LayoutParams params;
            params=new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            params.setMargins(0, 10, 0, 10); //left, top, right, bottom
            myButton.setLayoutParams(params);

            myButton.setBackgroundResource(R.color.button_bg);
            final int j = i;

            llButtons.addView(myButton);

            myButton.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    q.setUserAnswer(q.getAnswers().get(j));

                            showCorrectAnswers();

                        if(q.isCorrect(q.getUserAnswer()))
                        {
                            score++;
                        }

                        }

            });

        }

            pkt.setText("Pytanie: "+Integer.toString(ind)+"/"+Integer.toString(number_of_words)+"\n"+"Punkty: "+Integer.toString(score));

        llButtons.addView(pkt);

    }

    private void showCorrectAnswers() {
        final LinearLayout llButtons = (LinearLayout) findViewById(R.id.layout_scroll);
        final LinearLayout llQuestionText=(LinearLayout) findViewById(R.id.layout_text);
        Question q=questions.get(ind-1);

        for(int i=0;i<llButtons.getChildCount();i++) {
            if (llButtons.getChildAt(i) instanceof Button ) {

                Button myButton = (Button) llButtons.getChildAt(i);
                myButton.setOnClickListener(null);

                myButton.setTextSize(20);
                String buttonText = myButton.getText().toString();

                if (q.getUserAnswer().equals(buttonText))
                {
                    if(q.getCorrect().contains(buttonText))
                    {
                        myButton.setBackgroundResource(R.color.green);
                    }
                    else
                    {
                        myButton.setBackgroundResource(R.color.red);
                    }
                } else if(q.getCorrect().equals(buttonText)) {
                    myButton.setBackgroundResource(R.color.lightergreen);
                } else
                {
                    myButton.setBackgroundResource(R.color.button_bg);
                }
            } else llButtons.removeViewAt(i);

        }

        Button myButton = new Button(this);
        myButton.setBackgroundResource(R.color.button_bg);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        params.setMargins(0, 40, 0, 0);
        myButton.setLayoutParams(params);
        if (ind == number_of_words) //jak sie skoncza pytania to pokaz wyniki.
        {
            myButton.setText("Zakończ");
        } else {
            myButton.setText("Dalej");
        }
        myButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                llButtons.removeAllViewsInLayout();
                llQuestionText.removeAllViewsInLayout();
                if (ind == number_of_words) //jak sie skoncza pytania to pokaz wyniki.
                {
                    finish();
                } else {
                    update();
                }
            }
        });

        llButtons.addView(myButton);
        return;
    }

    private void generateNextButton()
    {

    }
}
