package com.nebnewt.learnjapanesevocabulary.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.os.Bundle;
import android.util.Log;

import com.nebnewt.learnjapanesevocabulary.R;
import com.nebnewt.learnjapanesevocabulary.networking.AsyncResponse;
import com.nebnewt.learnjapanesevocabulary.networking.InternetConnectionChecker;

import java.net.MalformedURLException;
import java.net.URL;


public class StartingActivity extends Activity implements AsyncResponse {
    URL url;
    InternetConnectionChecker logInChecker =new InternetConnectionChecker();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // setContentView(R.layout.activity_main);
        startActivity(new Intent(this, MenuActivity.class));
        finish();

       // logInChecker.delegate = this;
       // goToMenuActivity();
    }

    private void goToMenuActivity()
    {
        try{
            logInChecker.execute( new URL("http://japankitty.halamix2.pl/"));

        }catch (MalformedURLException ex)
        {

        }

    }

    @Override
    public void processFinish(Boolean hasInternetConnection) {
        Intent i;
            i = new Intent(this, MenuActivity.class);



        startActivity(i);
        overridePendingTransition(R.layout.fadein, R.layout.fadeout);

    }


    private boolean hasToken()
    {
        SharedPreferences sharedPreferences= PreferenceManager.getDefaultSharedPreferences(this);

        Log.i("TOKEN", Boolean.toString(sharedPreferences.getBoolean("token", false)));

        return (!sharedPreferences.getString("token", "error").equals("error"));

    }
}
