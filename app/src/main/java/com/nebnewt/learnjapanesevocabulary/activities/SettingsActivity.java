package com.nebnewt.learnjapanesevocabulary.activities;

import android.app.Activity;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;

import com.nebnewt.learnjapanesevocabulary.appobj.Constants;
import com.nebnewt.learnjapanesevocabulary.R;
import com.nebnewt.learnjapanesevocabulary.networking.DownloadedData;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SettingsActivity extends Activity {
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    @BindView(R.id.quiz) RadioButton radioButton;
    @BindView(R.id.hiraganaCheckbox) CheckBox checkBox;
    @BindView(R.id.polishToJapanese) RadioButton polishToJapaneseradioButton;
    @BindView(R.id.kanjiToKana) RadioButton kanjiToKanaRadioButton;
    @BindView(R.id.japaneseToPolish) RadioButton japaneseToPolishradioButton;
    @BindView(R.id.logout)
    Button logoutButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);



        editor = sharedPreferences.edit();

        editor.putString(Constants.direction, Constants.japaneseToPolish);

        if(DownloadedData.token==null)
        {
            logoutButton.setEnabled(false);
        }

        if(!sharedPreferences.getString(Constants.mode, Constants.flashcards).equals(Constants.flashcards))
        {
            radioButton.setChecked(true);
        }
        if(sharedPreferences.getBoolean(Constants.kana, false))
        {
            checkBox.setChecked(true);
        }
        if(sharedPreferences.getString(Constants.direction, Constants.defaultDirection).equals(Constants.polishToJapanese))
        {
            polishToJapaneseradioButton.setChecked(true);

        }
        else if(sharedPreferences.getString(Constants.direction, Constants.defaultDirection).equals(Constants.kanjiToKana))
        {
            kanjiToKanaRadioButton.setChecked(true);
        }

    }

    public void logout(View view)
    {
        DownloadedData.token=null;
        DownloadedData.userDetails=null;
//        SharedPreferences sharedpreferences= PreferenceManager.getDefaultSharedPreferences(this);
 //       SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("token", null);
        editor.commit();
        if(DownloadedData.token==null)
        {
            logoutButton.setEnabled(false);
        }
    }

    public void onModeRadioButtonClicked(View view) {

        boolean checked = ((RadioButton) view).isChecked();


        switch(view.getId()) {
            case R.id.flashcards:
                if (checked)
                    editor.putString(Constants.mode, Constants.flashcards);
                break;
            case R.id.quiz:
                if (checked)
                   editor.putString(Constants.mode, Constants.quiz);
                break;
        }
        editor.commit();
    }

    public void onKanaCheckboxClicked(View view)
    {
   //     editor=sharedPreferences.edit();
        boolean checked = ((CheckBox) view).isChecked();
        if(checked)
        {
            editor.putBoolean(Constants.kana, true);
        }
        else
        {
            editor.putBoolean(Constants.kana, false);
        }
        editor.commit();
    }

    public void onDirectionRadioButtonClicked(View view)
    {
        boolean checked = ((RadioButton) view).isChecked();
     //   editor = sharedPreferences.edit();
        switch(view.getId()) {
            case R.id.japaneseToPolish:
                if (checked)
                {
                    editor.putString(Constants.direction, Constants.japaneseToPolish);
               //     kanjiToKanaRadioButton.setChecked(false);

                }

                break;
            case R.id.polishToJapanese:
                if (checked)
                {
                    editor.putString(Constants.direction, Constants.polishToJapanese);
                  //  kanjiToKanaRadioButton.setChecked(false);
                }

                break;
            case R.id.kanjiToKana:
                if (checked)
                {
                    editor.putString(Constants.direction, Constants.kanjiToKana);
                 //   polishToJapaneseradioButton.setChecked(false);
                 //   japaneseToPolishradioButton.setChecked(false);
                }


                break;
        }
        editor.commit();
    }
}
