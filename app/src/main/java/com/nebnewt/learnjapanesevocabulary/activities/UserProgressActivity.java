package com.nebnewt.learnjapanesevocabulary.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.gson.Gson;
import com.nebnewt.learnjapanesevocabulary.appobj.Progress;
import com.nebnewt.learnjapanesevocabulary.R;
import com.nebnewt.learnjapanesevocabulary.networking.ProgressClient;
import com.nebnewt.learnjapanesevocabulary.networking.RetrofitInstance;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserProgressActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_progress);
        getUserProgress();
    }

    private void getUserProgress()
    {
        RetrofitInstance retrofitInstance=RetrofitInstance.getInstance();
        ProgressClient client =  retrofitInstance.getRetrofit().create(ProgressClient.class);

        Call<Progress> call =
                client.getProgress();

        call.enqueue(new Callback<Progress>() {
            @Override
            public void onResponse(Call<Progress> call, Response<Progress> response) {
                Log.i("mamTO", new Gson().toJson(response.body()));
            }

            @Override
            public void onFailure(Call<Progress> call, Throwable t) {
                Log.i("no dupa no", "nie da sie");
                //showNoInternetToast();

            }
        });

    }
}
