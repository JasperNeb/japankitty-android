package com.nebnewt.learnjapanesevocabulary.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.nebnewt.learnjapanesevocabulary.R;
import com.nebnewt.learnjapanesevocabulary.networking.AsyncResponse;
import com.nebnewt.learnjapanesevocabulary.networking.DownloadedData;
import com.nebnewt.learnjapanesevocabulary.networking.InternetConnectionChecker;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;


public class MenuActivity extends Activity implements AsyncResponse {

    InternetConnectionChecker internetConnectionChecker =new InternetConnectionChecker();
    @BindViews({ R.id.kanji, R.id.vocabulary }) List<Button> buttons;
    @BindView(R.id.options) Button optionsButton;
    @BindView(R.id.demo) Button categoriesButton;
    @BindView(R.id.goToDetails) Button detailsButton;
    @BindView(R.id.login) Button loginButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        ButterKnife.bind(this);

        internetConnectionChecker.delegate=this;

        SharedPreferences sharedPreferences= PreferenceManager.getDefaultSharedPreferences(this);
        DownloadedData.token=sharedPreferences.getString("token", null);

        try{

            internetConnectionChecker.execute( new URL("http://japankitty.halamix2.pl/"));


        }catch (MalformedURLException ex)
        {

        }

        disableButtons();


    }

    @Override
    protected void onResume()
    {
        super.onResume();
        if(DownloadedData.token!=null)
        {
            detailsButton.setEnabled(true);
            disableDemo();
            enableButtons();
        }
        else
        {
            loginButton.setEnabled(true);
            detailsButton.setEnabled(false);
            enableDemo();
            disableButtons();
        }


    }
    public void chooseVocabulary(View v)
    {
        Intent i=new Intent(this, CategoriesActivity.class);
        i.putExtra("type","vocabulary");
        startActivity(i);
    }
    public void chooseKanji(View v)
    {
        Intent i=new Intent(this, CategoriesActivity.class);
        i.putExtra("type","kanji");
        startActivity(i);
    }


    public void goToLoginActivity(View view)
    {
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(intent);
    }

    public void goToCategories(View view)
    {
        Intent intent = new Intent(getApplicationContext(), CategoriesActivity.class);
        intent.putExtra("type","vocabulary");

        startActivity(intent);
    }
    public void goToDetails(View view)
    {
        Intent intent = new Intent(getApplicationContext(), UserDetailsActivity.class);
        startActivity(intent);
    }


    public void goToSettingsActivity(View view)
    {
        Intent i= new Intent(this, SettingsActivity.class);
        startActivity(i);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.mainactivity_menu, menu);//Menu Resource, Menu
        return true;
    }



    @Override
    public void processFinish(Boolean output)
    {

        if(!output)
        {
           // changeButtonTexttoDemo();
            showNoInternetToast();
            disableButtons();
            loginButton.setEnabled(false);
            enableDemo();

        }
        else
        {
          //  changeButtonText();
            showConnectedToast();
            //disableDemo();
            loginButton.setEnabled(true);
            //enableButtons();
        }

    }

    private void showNoInternetToast()
    {
        int duration = Toast.LENGTH_SHORT;
        CharSequence text;
        text = "Brak internetu.";
        Toast toast = Toast.makeText(this, text, duration);
        toast.show();
    }

    private void showConnectedToast()
    {
        int duration = Toast.LENGTH_SHORT;
        CharSequence text;
        text = "Polaczono.";
        Toast toast = Toast.makeText(this, text, duration);
        toast.show();
    }

   private void changeButtonText()
    {
        categoriesButton.setText("Kategorie");
    }
    private void changeButtonTexttoDemo()
    {
        categoriesButton.setText("demo");
    }

    private void disableButtons()
    {
        for(Button b: buttons)
        {
            b.setEnabled(false);
        }
    }
    private void enableDemo()
    {
        categoriesButton.setEnabled(true);
    }
    private void disableDemo()
    {
        categoriesButton.setEnabled(false);
    }

    private void enableButtons()
    {
        for(Button b: buttons)
        {
            b.setEnabled(true);
        }
    }
}
