package com.nebnewt.learnjapanesevocabulary.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nebnewt.learnjapanesevocabulary.appobj.FlashCard2;
import com.nebnewt.learnjapanesevocabulary.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FlashCardActivity2 extends AppCompatActivity {

    Random rand;
    List <FlashCard2> flashCards;
    FlashCard2 currentFlashCard;


    @BindView(R.id.layout_buttons)
    LinearLayout layoutButtons;
    @BindView(R.id.question)
    TextView tv;
    @BindView(R.id.no_of_word)
    TextView no_of_word;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flash_card2);
        ButterKnife.bind(this);
        flashCards=new ArrayList();
        rand=new Random();



        currentFlashCard=flashCards.get(0);
    }

    public void flipFlashcard(View v) {
        currentFlashCard.flip();
    }

    private void drawThings() {
    }

    public void update()
    {
        int flashcardNumber;
        flashcardNumber=rand.nextInt(flashCards.size());

        final FlashCard2 flashCard=flashCards.get(flashcardNumber);
        tv.setText(flashCard.getShown());

       /* for(int i=1 ; i<flashCard.getElements().size() ; i++)
        {
            if(flashCard.getElements().get(i).equals(flashCard.getElements().get(0)))
            {
                continue;
            }
            final Button b= new Button(this);
            b.setBackgroundResource(R.color.button_bg);
            b.setText(flashCard.getLabels().get(i));
            final int iterator = i;
            b.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View w) {

                    b.setText(flashCard.getElements().get(iterator));

                }});
            layoutButtons.addView(b);

        }
    }

    @OnClick(R.id.ok)
    public void ok()
    {
        flashCards.remove(flashCards.get(rand));
        layoutButtons.removeAllViews();

        if(ind==number_of_words)
        {

            finish();
        }
        else
        {
            ind++;
            update();


        }


    }
    @OnClick(R.id.notok)
    public void notok()
    {
        layoutButtons.removeAllViews();

        Collections.shuffle(flashCards);
        update();
    }*/
    }
}
