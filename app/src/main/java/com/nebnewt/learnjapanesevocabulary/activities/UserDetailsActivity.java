package com.nebnewt.learnjapanesevocabulary.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;
import com.nebnewt.learnjapanesevocabulary.R;
import com.nebnewt.learnjapanesevocabulary.appobj.UserDetails;
import com.nebnewt.learnjapanesevocabulary.networking.DownloadedData;
import com.nebnewt.learnjapanesevocabulary.networking.GetAsyncUserDetails;
import com.nebnewt.learnjapanesevocabulary.networking.GetUserDetailsCallbackInterface;
import com.nebnewt.learnjapanesevocabulary.networking.QueryData;

public class UserDetailsActivity extends AppCompatActivity implements GetUserDetailsCallbackInterface {

    UserDetails userDetails=null;
    static String data = null;


     TextView usernameTextview;

     TextView sexTextview;
    TextView createdTextview;
     TextView updatedAtTextview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);
        usernameTextview=findViewById(R.id.username);
        sexTextview=findViewById(R.id.sex);
        createdTextview=findViewById(R.id.createdAt);
        updatedAtTextview=findViewById(R.id.updatedAt);
        getDetailsFromInternet();

    }

    private void getDetailsFromInternet()
    {
        try
        {
            QueryData dane = new QueryData();
            GetAsyncUserDetails fuckingAsync = new GetAsyncUserDetails(this);
            fuckingAsync.execute(dane);
        }
        catch (Exception e)
        {
            Log.e("mam", e.getMessage());
        }
    }

    @Override
    public void fetchDataCallback(String result) {
        data=result;
        Log.i("mam w IDA", data);

        convertToUserDetails();
        fillViews();
    }

    private void convertToUserDetails()
    {
        Gson gson=new Gson();
        DownloadedData.userDetails=gson.fromJson(data, UserDetails.class);
        Log.i("mam", DownloadedData.userDetails.getName());

    }

    private void fillViews()
    {

        usernameTextview.setText("Imię: "+DownloadedData.userDetails.getName());
        sexTextview.setText("Płeć: "+DownloadedData.userDetails.getSex());
        createdTextview.setText("Utworzono: "+DownloadedData.userDetails.getCreated_at());
        updatedAtTextview.setText("Ostatnia modyfikacja: "+DownloadedData.userDetails.getUpdated_at());
    }

    public void goToProgress(View view)
    {
        Intent i;
        i=new Intent(this, UserProgressActivity.class);
        startActivity(i);
    }
}
