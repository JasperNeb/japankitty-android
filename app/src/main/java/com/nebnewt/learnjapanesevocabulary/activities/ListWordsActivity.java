package com.nebnewt.learnjapanesevocabulary.activities;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nebnewt.learnjapanesevocabulary.jsonhandling.JSONHandler;
import com.nebnewt.learnjapanesevocabulary.R;
import com.nebnewt.learnjapanesevocabulary.appobj.Word;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ListWordsActivity extends Activity {
    ArrayList<Word> words;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_words);
        words=new ArrayList<>();
        String filename = getIntent().getExtras().getString("FILENAME");

        load(filename);
        listWords();

    }
    private void load(String filename)
    {
        try { //need to move this things to another class
            JSONArray questionsJSONArray = JSONHandler.readJSONArrayFromAssets("categories/"+filename, getApplicationContext());

            for (int i = 0; i < questionsJSONArray.length(); i++)
            {
                JSONObject tempObject = questionsJSONArray.getJSONObject(i);

                String tempKanji = tempObject.getString("kanji");
                String tempPolish = tempObject.getString("polish");
                String tempKana = tempObject.getString("hiragana");

                Word w=new Word(i, tempKana, tempKanji, tempPolish);

                words.add(w);
            }

        } catch (JSONException e) {
            Log.e("error", e.getMessage());
        }
    }

    private void listWords()
    {
        LinearLayout ll=(LinearLayout)findViewById(R.id.listOfWords);

        for(int i=0 ; i<words.size() ; i++)
        {
            TextView tv=new TextView(this);
            tv.setTextSize(15);

            tv.setBackgroundResource(R.drawable.text);
            //// TODO: 04.11.2017 will not work for hiragana 
            tv.setText(words.get(i).getKanji()+"   "+words.get(i).getKana()+"   "+words.get(i).getPolish()); //or override?
            ll.addView(tv);


        }


    }
}
