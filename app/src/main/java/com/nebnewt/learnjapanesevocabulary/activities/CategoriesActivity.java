package com.nebnewt.learnjapanesevocabulary.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nebnewt.learnjapanesevocabulary.appobj.Constants;
import com.nebnewt.learnjapanesevocabulary.appobj.Course;
import com.nebnewt.learnjapanesevocabulary.jsonhandling.JSONHandler;
import com.nebnewt.learnjapanesevocabulary.R;
import com.nebnewt.learnjapanesevocabulary.networking.RetrofitInstance;
import com.nebnewt.learnjapanesevocabulary.networking.CategoriesClient;
import com.nebnewt.learnjapanesevocabulary.networking.DownloadedData;

import org.json.JSONArray;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoriesActivity extends Activity {

    @BindView(R.id.waitingTextView)
    TextView waitingTextView;
    private int number_of_questions = 5;
    SharedPreferences sharedpreferences;
    ArrayList<Course> courses;
    private Boolean online=false;
    String type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        sharedpreferences = PreferenceManager.getDefaultSharedPreferences(this);
        type=getIntent().getExtras().getString("type");


        if(DownloadedData.token!=null)
        {
            getCategoriesFromInternet();
            Log.i("CatsActivity", "z neta");

        }
        else
        {
            Log.i("CatsActivity", "nie z neta");
            generateButtonsArrayFromFile();
            generatebuttons();
        }


    }

    private void getCategoriesFromInternet()
    {

        RetrofitInstance retrofitInstance = RetrofitInstance.getInstance();

        CategoriesClient client =  retrofitInstance.getRetrofit().create(CategoriesClient.class);

        Call<List<Course>> call =
                client.coursesList(type);

        call.enqueue(new Callback<List<Course>>() {
            @Override
            public void onResponse(Call<List<Course>> call, Response<List<Course>> response) {
                Log.i("dupa", new Gson().toJson(response.body()));

                courses=new ArrayList<Course>(response.body());
                online=true;
                generatebuttons();


            }

            @Override
            public void onFailure(Call<List<Course>> call, Throwable t) {
                Log.i("no dupa no", "nie da sie");
                showNoInternetToast();
                generateButtonsArrayFromFile();
                generatebuttons();


            }
        });
    }

    private void generatebuttons() {
        waitingTextView.setVisibility(View.GONE);
        LinearLayout ll = (LinearLayout) findViewById(R.id.layout_buttons);

        int i;
        for (i = 0; i < courses.size(); i++) {
            Log.i("id", Integer.toString(courses.get(i).getId()));
            Button myButton = new Button(this);
            myButton.setBackgroundResource(R.color.button_bg);
            myButton.setText(courses.get(i).getName());
            LinearLayout.LayoutParams params;
            params=new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            params.setMargins(0, 10, 0, 10); //left, top, right, bottom
            myButton.setLayoutParams(params);
            final int fileIterator = i;
            myButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View w) {

                        Intent intent;

                        if (sharedpreferences.getString(Constants.mode, "flashcards").equals("quiz")) {
                            intent = new Intent(getApplicationContext(), QuizActivity.class);
                            Log.i("spoczi", "jest quiz");
                        } else {
                            intent = new Intent(getApplicationContext(), FlashCardActivity.class);
                        }
                        intent.putExtra("id", Integer.toString(courses.get(fileIterator).getId()));
                        intent.putExtra("FILENAME", courses.get(fileIterator).getFile());
                        Log.i("filename", courses.get(fileIterator).getFile());
                        intent.putExtra("number_of_questions", number_of_questions);
                        intent.putExtra("TYPE", courses.get(fileIterator).getType());
                        intent.putExtra("online", online);
                        startActivity(intent);


                }
            });
            myButton.setAlpha((float) 0.8);

            ll.addView(myButton, params);
        }


    }

    private void generateButtonsArrayFromFile()
    {
        Gson gson=new Gson();
        final JSONArray categoriesList = JSONHandler.readJSONArrayFromAssets("categories.json", getApplicationContext());
        Type listType = new TypeToken<List<Course>>(){}.getType();
        courses=new ArrayList<>();
        courses = gson.fromJson(categoriesList.toString(), listType);
    }

    private void showNoInternetToast()
    {
        int duration = Toast.LENGTH_SHORT;
        CharSequence text;
        text = "Brak internetu. Wyswietlono kategorie trybu offline";
        Toast toast = Toast.makeText(this, text, duration);
        toast.show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.mainactivity_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        MenuItem menuItem1 = (MenuItem) findViewById(R.id.kana);
        // Handle item selection
        switch (item.getItemId()) {
        /*    case R.id.no_5:
                editor.putInt("no_of_questions", 5);
                editor.commit();
                return true;
            case R.id.no_10:
                editor.putInt("no_of_questions", 10);
                editor.commit();
                return true;
            case R.id.no_15:
                editor.putInt("no_of_questions", 15);
                editor.commit();
                return true;
            case R.id.infinity:
                // editor.putInt("no_of_questions", );
                // editor.commit();
                return true;*/
            case R.id.kana:
                if (sharedpreferences.getBoolean("kana", true)) {
                    editor.putBoolean("kana", false);
                    // menuItem1.setChecked(false);
                } else {
                    //menuItem1.setChecked(true);
                    editor.putBoolean("kana", true);
                }
                editor.commit();
                return true;
            case R.id.flashcards:
                editor.putString("mode", "flashcards");
                editor.commit();
                return true;
            case R.id.quiz:
                editor.putString("mode", "quiz");
                editor.commit();
                return true;
            case R.id.japaneseToPolish:
                editor.putBoolean("polishToJapanese", false);
                editor.putBoolean("kanjiToKana", false);
                editor.commit();
                return true;
            case R.id.polishToJapanese:
                editor.putBoolean("polishToJapanese", true);
                editor.putBoolean("kanjiToKana", false);
                editor.commit();
                return true;
           // case R.id.kanjitokana:
                //   editor.putBoolean("kanjiToKana", true);
                //  editor.commit();
         //       return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}

