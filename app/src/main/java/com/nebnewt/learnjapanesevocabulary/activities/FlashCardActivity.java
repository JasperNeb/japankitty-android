package com.nebnewt.learnjapanesevocabulary.activities;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.google.gson.Gson;
import com.nebnewt.learnjapanesevocabulary.appobj.FlashCard;
import com.nebnewt.learnjapanesevocabulary.jsonhandling.JSONHandler;
import com.nebnewt.learnjapanesevocabulary.appobj.Kanji;
import com.nebnewt.learnjapanesevocabulary.networking.KanjiClient;
import com.nebnewt.learnjapanesevocabulary.R;
import com.nebnewt.learnjapanesevocabulary.networking.RetrofitInstance;
import com.nebnewt.learnjapanesevocabulary.appobj.Word;
import com.nebnewt.learnjapanesevocabulary.networking.WordsClient;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FlashCardActivity extends Activity { //TODO SENSOWNY WYBOR SLOWEK >.<

    private SharedPreferences sharedPreferences;

    private Random r;


    private FlashCard current_flashcard;
    private ArrayList<FlashCard> flashCards;
    private int ind=1;
    private int rand;
    private int number_of_words;
    private boolean kana;
    private String type;
    private String id;

    @BindView(R.id.layout_buttons)
    LinearLayout layoutButtons;
    @BindView(R.id.question)
    TextView tv;
    @BindView(R.id.no_of_word)
    TextView no_of_word;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flash_card);
        ButterKnife.bind(this);


        type=getIntent().getExtras().getString("TYPE");

        r=new Random();

        sharedPreferences= PreferenceManager.getDefaultSharedPreferences(this);

        flashCards=new ArrayList<>();
        String filename = getIntent().getExtras().getString("FILENAME");
        // number_of_words = getIntent().getExtras().getInt("number_of_questions");

        number_of_words = sharedPreferences.getInt("no_of_questions", 5);
        kana = sharedPreferences.getBoolean("kana", true);

        id = getIntent().getExtras().getString("id");
        if(getIntent().getExtras().getBoolean("online"))
        {
            loadFromInternet();
        }
        else
        {
            load(filename);
        }


    }
    private void loadFromInternet()
    {
        RetrofitInstance retrofitInstance=RetrofitInstance.getInstance();


        if(type.equals("kanji"))
        {
            Log.i("typee to", "kanji");
            KanjiClient client =  retrofitInstance.getRetrofit().create(KanjiClient.class);

            Call<List<Kanji>> call =
                    client.kanjiList(id);

            call.enqueue(new Callback<List<Kanji>>() {
                @Override
                public void onResponse(Call<List<Kanji>> call, Response<List<Kanji>> response) {
                    Log.i("dupa3434", new Gson().toJson(response.body()));

                    ArrayList<Kanji> kanji;
                    kanji=new ArrayList<Kanji>(response.body());

                    kanjiToFlashCard(kanji);
                    update();



                }

                @Override
                public void onFailure(Call<List<Kanji>> call, Throwable t) {
                    tv.setText("Wystąpił problem z pobraniem danych z serwera.");



                }
            });
        }
        else if(type.equals("vocabulary"))
        {
            WordsClient client =  retrofitInstance.getRetrofit().create(WordsClient.class);

            Call<List<Word>> call =
                    client.wordsList(id);

            call.enqueue(new Callback<List<Word>>() {
                @Override
                public void onResponse(Call<List<Word>> call, Response<List<Word>> response) {
                    Log.i("dupa", new Gson().toJson(response.body()));

                    ArrayList <Word> words=new ArrayList<Word>(response.body());
                    for(Word w : words)
                    {
                        FlashCard flashCard = new FlashCard(w);
                        flashCards.add(flashCard);
                    }
                    update();



                }

                @Override
                public void onFailure(Call<List<Word>> call, Throwable t) {
                    tv.setText("Wystąpił problem z pobraniem danych z serwera.");




                }
            });
        }
    }

    private void kanjiToFlashCard(ArrayList<Kanji> kanjis)
    {
        for(Kanji k : kanjis)
        {
            Log.i("response", k.toString());
            FlashCard flashCard= new FlashCard(k);
            flashCards.add(flashCard);
        }
    }
    private void load(String filename)
    {
        try {
            JSONArray questionsJSONArray = JSONHandler.readJSONArrayFromAssets("categories/"+filename, getApplicationContext());

            if(type.equals("words") || type.equals("kana"))
            {
                ArrayList<Word> words;
                words=JSONHandler.JsonToWord(questionsJSONArray);
                for(Word w : words)
                {
                    FlashCard flashCard = new FlashCard(w);
                    flashCards.add(flashCard);
                }
            }
            else if(type.equals("kanji"))
            {
                ArrayList<Kanji> kanji;

                kanji=JSONHandler.JsonToKanji(questionsJSONArray);
                for(Kanji k : kanji)
                {
                    FlashCard flashCard= new FlashCard(k);
                    flashCards.add(flashCard);
                }
            }


            Collections.shuffle(flashCards);
            number_of_words=flashCards.size();


        } catch (JSONException e) {
            Log.e("error", e.getMessage());
        }
    }


    private void update()
    {
        rand=r.nextInt(flashCards.size());

        final FlashCard flashCard=flashCards.get(rand);
        tv.setText(flashCard.getElements().get(0));

        for(int i=1 ; i<flashCard.getElements().size() ; i++)
        {
            if(flashCard.getElements().get(i).equals(flashCard.getElements().get(0)))
            {
                continue;
            }
            final Button b= new Button(this);
            b.setBackgroundResource(R.color.button_bg);
            b.setText(flashCard.getLabels().get(i));
            final int iterator = i;
            b.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View w) {

                    b.setText(flashCard.getElements().get(iterator));

                }});
            layoutButtons.addView(b);

        }
    }

    @OnClick(R.id.ok)
    public void ok()
    {
        flashCards.remove(flashCards.get(rand));
        layoutButtons.removeAllViews();

        if(ind==number_of_words)
        {

            finish();
        }
        else
        {
            ind++;
            update();


        }


    }
    @OnClick(R.id.notok)
    public void notok()
    {
        layoutButtons.removeAllViews();

        Collections.shuffle(flashCards);
        update();
    }

}
