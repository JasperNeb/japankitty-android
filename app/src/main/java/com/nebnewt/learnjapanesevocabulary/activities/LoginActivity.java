package com.nebnewt.learnjapanesevocabulary.activities;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.nebnewt.learnjapanesevocabulary.R;
import com.nebnewt.learnjapanesevocabulary.networking.RetrofitInstance;
import com.nebnewt.learnjapanesevocabulary.appobj.User;
import com.nebnewt.learnjapanesevocabulary.networking.UserClient;
import com.nebnewt.learnjapanesevocabulary.networking.DownloadedData;
import com.nebnewt.learnjapanesevocabulary.networking.LoginData;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends Activity {

    String API_BASE_URL = "http://dev.japankitty.halamix2.pl/api/v1/";
    String ts;

    @BindView(R.id.login)
    EditText textView;
    @BindView(R.id.password)
    EditText textViewPasswd;
    @BindView(R.id.alreadyLoggedIn)
    TextView textViewAlreadyLoggedIn;
    @BindView(R.id.login_button)
    Button loginButton;

    SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        sharedPreferences=PreferenceManager.getDefaultSharedPreferences(this);

        if(DownloadedData.token!=null)
        {
            loginButton.setEnabled(false);
            textViewAlreadyLoggedIn.setText("Jesteś już zalogowany");
        }

    }
    public void onLoginClick(View view)
    {
        String email = "admin@admin.pl";
        String password = "admin";
        email=textView.getText().toString();
        password=textViewPasswd.getText().toString();
        Log.i("ochu","logujemy!");

        /*OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        Retrofit.Builder builder =
                new Retrofit.Builder()
                        .baseUrl(API_BASE_URL)
                        .addConverterFactory(
                                GsonConverterFactory.create()
                        );

        Retrofit retrofit =
                builder
                        .client(
                                httpClient.build()
                        )
                        .build();*/
        RetrofitInstance retrofitInstance=RetrofitInstance.getInstance();

        UserClient client =  retrofitInstance.getRetrofit().create(UserClient.class);

        final LoginData loginData = new LoginData(email,password);
        Call<User> call = client.loginUser(loginData);

        Log.i("transmisja",call.getClass().getName());
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {

                Log.i("mam", response.toString());
                if(response.code()!=200)
                {
                    textViewAlreadyLoggedIn.setText("Błąd: "+ Integer.toString(response.code()));
                }
                else
                {
                    Log.i("mam tok",  response.body().getToken());
                    DownloadedData.token="Bearer "+response.body().getToken();
                    createToken();
                    finish();
                }



            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                showNoInternetToast();
            }
        });
    }


    private void showNoInternetToast()
    {
        int duration = Toast.LENGTH_SHORT;
        CharSequence text;
        text = "Brak internetu. Wyswietlono kategorie trybu offline";
        Toast toast = Toast.makeText(this, text, duration);
        toast.show();
    }
    private void createToken()
    {
        SharedPreferences sharedpreferences= PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("token", "Bearer+anything");
        editor.commit();

    }

}
